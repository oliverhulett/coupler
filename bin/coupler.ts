#!/usr/bin/env node

import process from 'process';
import script from '../lib/script';

process.exit(script.main(process.argv.slice(2)));
