#!/bin/bash
##
##	Wrapper around `{%= task_runner.wrapped_cmd_string %}` from {%= task_runner.url %}
##	Ensure CWD is top level of the project.
##	Source init.sh to install {%= task_runner.name %}.  This will actually install all the required (development time) dependencies for
##	the project, but you'll probably need them eventually anyway.
##	Then forward all arguments `{%= task_runner.wrapped_cmd_string %}`.
set -e

HERE="$(cd "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)"
source "${HERE}/init.sh"
(cd "${HERE}" && {% task_runner.command_line %} "$@")
