# shellcheck shell=bash
##
##	This file is intended to be sourced.  It will setup a development environment for this project.
##	This file is as idempotent as possible, so it should be able to be run multiple times without ill effect.  Subsequent runs should be very fast.
##
set -e

HERE="$(cd "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)"
EXE="$(cd "$(dirname -- "$0")" && pwd -P)/$(basename -- "$0")"
VENV_DIR="${HERE}/.venv"

GREEN="$(tput -T"${TERM:-dumb}" setaf 2 || true)"
WHITE="$(tput -T"${TERM:-dumb}" bold || true)$(tput -T"${TERM:-dumb}" setaf 7 || true)"
RESET="$(tput -T"${TERM:-dumb}" sgr0 || true)"
function _tell()
{
	echo "${WHITE} $* ${RESET}"
}
function _success()
{
	echo "${GREEN} $* ${RESET}"
}

_CHANGED="false"

# Requirements for the development environment and testing only.  Runtime dependencies should be listed in setup.py.
read -r -d '' REQUIREMENTS <<-'EOF'
EOF

if [ ! -e "${VENV_DIR}/bin/activate" ]; then
	_tell "Creating python virtual environment: ${VENV_DIR}"
	_CHANGED="true"
	virtualenv -p "$(command which python)" --prompt="(.venv:{%= project.name %}) " "${VENV_DIR}" || return 1
fi

if [ "${VIRTUAL_ENV}" != "${VENV_DIR}" ]; then
	if [ "${EXE}" != "${HERE}/{%= task_runner.name %}" ]; then
		# Don't advertise if we've been sourced from ./{%= task_runner.name %}, you can't take it with you.
		_tell "Activating python virtual environment: ${VENV_DIR}"
		_CHANGED="true"
	fi
	# shellcheck source=.venv/bin/activate
	# shellcheck disable=SC1091
	source "${VENV_DIR}/bin/activate"
fi

if [ ! -e "${VENV_DIR}/requirements.txt" ] || [ "$(command cat "${VENV_DIR}/requirements.txt")" != "${REQUIREMENTS}" ]; then
	_tell "Installing python development requirements: ${VENV_DIR}/requirements.txt"
	_CHANGED="true"
	echo "${REQUIREMENTS}" >"${VENV_DIR}/requirements.txt"
	pip install -q -r "${VENV_DIR}/requirements.txt"
fi

if [ ! -d "${HERE}/node_modules" ] || [ ! -e "${HERE}/package-lock.json" ] || [ "${HERE}/package.json" -nt "${HERE}/package-lock.json" ]; then
	_tell "Installing nodejs development requirements: ${HERE}/package.json"
	_CHANGED="true"
	(cd "${HERE}" && npm install)
	if [ -e "${HERE}/package-lock.json" ]; then
		# Ensure package-lock.json is newer than package.json so that we don't run `npm install` unnecessarily.
		touch "${HERE}/package-lock.json"
	fi
fi

if [ "${_CHANGED}" == "true" ]; then
	# Don't advertise if we haven't changed anything, the dev env was already set up.
	_success "{%= project.pretty_name %} environment set up"
fi
