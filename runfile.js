let { run, options, help } = require('runjs');
let utils = require('./tasks/utils');


// The argument handling is a little shit...
// To pass an argument to a command line option you have to make it a single token and use an equals sign.  (e.g. --port=8080)
// Having the argument as the next token (e.g. -p 8080 or --port 8080) doesn't work, 8080 shows up as an unnamed argument and the -p or --port flags are set to "true"
// Is there some clever wrapping we can do to handle this?  Otherwise can we wrap task creation or help[options] to warn users?  Also, look at the underlying library and runjs' handling, maybe it can be improved?


// TODO: replace with file.read('package.json')
let VERSION = '1.0.0';
module.exports = [
    VERSION,
];

function build() {
    // Order is important (ish)
    run('npm install');
    run('tsc --build');
    //run('npx run docker:build');
}

help(build, {
    description: 'Build the project',
});
module.exports.build = build;


function test() {
    let myOpts = {
        grep: options(this).grep || options(this).g,
        reporter: options(this).reporter || options(this).r,
        slow: options(this).slow || options(this).s,
    };
    let grep_arg = '';
    if (myOpts.grep) {
        grep_arg = ' --grep ' + myOpts.grep;
    }
    let reporter_arg = ' --reporter list';
    if (myOpts.reporter) {
        reporter_arg = ' --reporter ' + myOpts.reporter;
    }
    let slow_arg = '';
    if (myOpts.slow) {
        slow_arg = ' --slow ' + myOpts.slow;
    }
    run('mocha --use_strict --check-leaks --recursive tests' + grep_arg + reporter_arg + slow_arg);
}

help(test, {
    description: 'Test the project',
    options: {
        grep: "A grep expression for tests to run, greps part of the test description.",
        g: "A grep expression for tests to run, greps part of the test description.",
        reporter: "The output format reporter to use",
        r: "The output format reporter to use",
        slow: "The time theshold for considering a test slow (ms)",
        s: "The time theshold for considering a test slow (ms)",
    },

});
module.exports.test = test;


function runProject(...args) {
    run('rm -rf build/project');
    run('mkdir -p build/project');
    run('./bin/coupler.js --stack --debug --verbose' + args.join(' '));
}

help(runProject, {
    description: 'Run the project',
});
module.exports.run = runProject;


function format() {
    let myOpts = {
        verbose: options(this).verbose || options(this).v,
    };
    // The double bash hackery is because the underlying shell for `run()` is /bin/sh, which parses but doesn't understand the '<()' command substitution.
    run('bash -c "bash <(curl -sS https://bitbucket.org/oliverhulett/formatter/raw/master/run.sh)' + utils.bflag(myOpts.verbose, '--verbose') + 'format --exclude package-lock.json"');
}

help(format, {
    description: 'Run formatter in format mode on applicable files in this project.',
    options: {
        v: "Run in verbose mode",
        verbose: "Run in verbose mode",
    },
});
module.exports.format = format;

function check() {
    let myOpts = {
        verbose: options(this).verbose || options(this).v,
    };
    // The double bash hackery is because the underlying shell for `run()` is /bin/sh, which parses but doesn't understand the '<()' command substitution.
    run('bash -c "bash <(curl -sS https://bitbucket.org/oliverhulett/formatter/raw/master/run.sh)' + utils.bflag(myOpts.verbose, '--verbose') + 'check --exclude package-lock.json"');
}

help(check, {
    description: 'Run formatter in check mode for applicable files in this project.',
    options: {
        v: "Run in verbose mode",
        verbose: "Run in verbose mode",
    },
});
module.exports.check = check;


function clean() {
    let myOpts = {
        all: options(this).all || options(this).a,
    };
    //run('npx run docker:clean' + utils.bflag(myOpts.verbose, '--all'));
        let docs = [
            "build",
        ];
        for (i in docs) {
            run('rm -rf ' + docs[i]);
        }
    if (myOpts.all) {
        let docs = [
            ".venv",
            "node_modules",
        ];
        for (i in docs) {
            run('rm -rf ' + docs[i]);
        }
    }
}
help(clean, {
    description: 'Clean files and folders that are created by the build processes.',
    options: {
        a: "Clean development dependencies as well",
        all: "Clean development dependencies as well",
    },
});
module.exports.clean = clean;

// Uncomment to include docker related tasks.
//module.exports.docker = require('./tasks/docker');
