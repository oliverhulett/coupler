let fs = require("fs");
let path = require("path");

function _find_modules(dir) {
    let _mods = {};
    fs.readdir(dir, function(err, files) {
        if (err) throw err;
        files.forEach(function(file) {
            let filepath = path.join(dir, file);
            fs.stat(filepath, function(err,stats) {
                if (stats.isDirectory()) {
                    _mods[file] = _find_modules(filepath);
                } else if (stats.isFile()) {
                    if (file.substring(file.length - 3) == '.js') {
                        _mods[file.substring(0, file.length - 3)] = require(filepath);
                    }
                }
            });
        });
    });
    return _mods;
}

module.exports = _find_modules(path.join(path.dirname(fs.realpathSync(__filename)), 'lib'));
