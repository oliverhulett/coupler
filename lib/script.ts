import dedent from 'dedent';
import fs from 'fs';
import path from 'path';
import process from 'process';
import yargs from 'yargs';


function readConfigFile(configFile: string) {
}

function validatePathDir(dir: string) {
}

function addOptions(args: yargs) {
    return args
       .option(
           "show-completion-script",
           {
               boolean: true,
               description: "Print a bash completion script for this command.",
           },
       )
       .option(
           "no-colour",
           {
               alias: ['no-color'],
               boolean: true,
               description: "Disable colored output.  Passed through to grunt-init.",
           },
       )
       .option(
           "stack",
           {
               boolean: true,
               description: "Print a stack trace when exiting with a warning or fatal error.  Passed through to grunt-init, not used by coupler.",
           },
       )
       .option(
           "project-dir",
           {
               alias: ['C'],
               normalize: true,
               default: process.cwd(),
               description: "Change into the given directory first, by default run in the CWD.",
           },
       )
       .option(
           "config",
           {
               alias: ['c'],
               config: true,
               array: true,
               default: [/*'~/.couplerrc'*/],
               description: "Add configuration/defaults file.  Can be specified more than once.",
           },
       )
       .option(
           "debug",
           {
               alias: ['d'],
               boolean: true,
               description: "Enable debugging mode for tasks that support it.  Passed through to grunt-init.",
           },
       )
       .option(
           "no-write",
           {
               alias: ['n'],
               boolean: true,
               description: "Disable writing files (dry run).  Passed through to grunt-init.",
           },
       )
       .option(
           "path",
           {
               alias: ['p'],
               normalize: true,
               array: true,
               default: [path.join(path.dirname(fs.realpathSync(__filename)), '..', 'fittings')],
               description: "Add a directory to the coupler search path.  See the README for an explanation of the path and its contents.",
           },
       )
       .option(
           "quiet",
           {
               alias: ['q'],
               boolean: true,
               description: "Don't ask questions.  Coupler will infer what it can, re-use previous answers, and use defaults for the rest.",
           },
       )
       .option(
           "timid",
           {
               alias: ['t'],
               boolean: true,
               description: "Always ask questions.  Coupler will not infer or re-use previous answers.",
           },
       )
       .option(
           "verbose",
           {
               alias: ['v'],
               count: true,
               description: "Verbose mode. A lot more informational output.  Passed through to grunt-init.",
           },
       )
}

function addCommands(args: yargs) {
//           .command(
//               "project",
//               "Create or update a project from templates composed from fittings",
//               // TODO: Farm this out to the project module
//           )
//           .command(
//               "tree",
//               "Create or update a source tree in a project, also from templates composed from fittings",
//               // TODO: Farm this out to the tree module
//           )
//           .command(
//               "object",
//               "Create a source object in a source tree, also from templates composed from fittings.",
//               // TODO: Farm this out to the source object module
//           )
}

export function main(argv: [string]) {
    // parse args
    let args = yargs
        .wrap(yargs.terminalWidth() - 2)
        .version()
        .usage(
            "$0 <global options> project|tree|ob[ject] <local options>",
            "Create (or update) from a template.  You can create a project, a source tree, or a source object.",
        )
        .epilogue("See the README at https://bitbucket.org/oliverhulett/coupler/src/master/ for more information.")
        .help()
        .completion();

    addOptions(args);
    addCommands(args);

    args.demandCommand(1, 1, "Must specify a command to run")
    args.recommendCommands()
    let settings = args.parse(argv);

        this.log.dump("parsed args", settings);
        process.exit(0);

        // Read config files
        for (c in this.settings.CONFIG) {
            this.readConfigFile(c);
        }

        // Validate PATH
        for (p in this.settings.PATH) {
            this.validatePathDir(p);
        }

        // Get type of thing to create from args
        if (!argv.hasOwnProperty('_') || argv['_'].length == 0) {
            if (!argv['help']) {
                this.settings.warn("Sub-command required");
            }
            this._printHelp(this.settings.info);
            return !argv['help'];
        }
        let cmd = argv['_'].pop(1);
        let type = null;
        let typeName = null;
        let lib = path.dirname(fs.realpathSync(__filename));
        if (cmd == 'project') {
            type = require(path.join(lib, 'project'));
            typeName = 'Project';
        } else if (cmd == 'tree') {
            type = require(path.join(lib, 'tree'));
            typeName = 'Source Tree';
        } else if (cmd == 'obj' || cmd == 'object') {
            type = require(path.join(lib, 'object'));
            typeName = 'Source Object';
        } else {
            this.settings.warn("Sub-command not recognised");
            this._printHelp(this.settings.info);
            return 1;
        }

        if (argv['help']) {
            this.settings.dump(`printing help: ${typeName}`, this);
            type.help(this.settings, argv['_']);
            return 0;
        } else {
            this.settings.dump(`running command ${typeName}`, this);
            this.settings.verbose(dedent(`
                Orchestrating creation:
                  type: ${typeName}
                  cwd: ${this.settings.WORKING_DIR}
                  colour: ${this.settings.COLOUR}
                  dry run: ${this.settings.DRY_RUN}
                  quiet: ${this.settings.QUIET}
                  timid: ${this.settings.TIMID}
                  config files: ${this.settings.CONFIG.join(', ')}
                  coupler path: ${this.settings.PATH.join(', ')}
                  verbose: ${this.settings.VERBOSE}
                  debug: ${this.settings.DEBUG}
                  stack: ${this.settings.STACK}
            `));
            return type.main(this.settings, argv['_']);
        }
    },
};
