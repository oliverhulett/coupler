let argParser = require('minimist');
let dedent = require('dedent');
let fs = require('fs');
let path = require('path');

module.exports = {
    _printHelp: function(logger=console.log) {
        logger(dedent(`
            coupler <global options> tree <language> [<root>]
                Create (or update) a source tree.  Coupler will discover fittings, ask questions, and update the filesystem under the CWD from the templates it finds and the answers you give.
                A source tree is like a sub-project.  It will have its own source files and test files in the idiomatic heirarchy for its programming language and maybe also its own build tooling.
                As well as seting up the idomatic file heirarchy, this command will initiallise any tooling that will be common to the source objects within a source tree, and hook into the project wide orchestration tooling.
        `));
    },

    help: function(settings, argv) {
        this._printHelp();
        return 0;
    },

    main: function(settings, args) {
        settings.verbose(`Creating source tree for project in ${settings.WORKING_DIR}/`);
        // parse args
        //none yet

        // find fittings

        // find existing project settings

        // Orchestrate grunt-init
        return 0;
    },
};
