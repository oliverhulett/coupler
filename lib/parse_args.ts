let dedent = require('dedent');
let fs = require('fs');
let path = require('path');
let process = require('process');
let yargs = require('yargs');

// TODO:  Switch to `yargs`
module.exports = {
    _doArrayOpt: function(opts, arg, desc, key) {
        if (desc.hasOwnProperty(key) && desc[key]) {
            if (!opts.hasOwnProperty(key)) {
                opts[key] = [];
            }
            opts[key].push(arg);
        }
    },

    _doDictOpt: function(opts, arg, desc, key) {
        if (desc.hasOwnProperty(key)) {
            if (!opts.hasOwnProperty(key)) {
                opts[key] = {};
            }
            opts[key][arg] = desc[key];
        }
    },

    parseArgs: function (argv, expectedArgs={}, stopEarly=false, unknown=null) {
        let opts = {
            stopEarly: stopEarly,
            unknown: unknown,
        };
        for (let a in expectedArgs) {
            let desc = expectedArgs[a];
            this._doArrayOpt(opts, a, desc, 'boolean');
            this._doArrayOpt(opts, a, desc, 'string');
            if (desc.hasOwnProperty('aliases')) {
                desc['alias'] = desc['aliases'];
            }
            this._doDictOpt(opts, a, desc, 'default');
            this._doDictOpt(opts, a, desc, 'alias');
        }

        let results = argParser(argv, opts=opts);

        for (let a in expectedArgs) {
            let desc = expectedArgs[a];
            let fn = (r, p) => { return r.hasOwnProperty(p); };
            if (desc.hasOwnProperty('boolean') && desc['boolean']) {
                fn = (r, p) => { return r[p]; };
            }
            if (fn(results, a) && desc.hasOwnProperty('action')) {
                desc['action'](results[a]);
            } else if (!fn(results, a) && desc.hasOwnProperty('onMissing')) {
                desc['onMissing']();
            }
        }
        return results;
    },
};
