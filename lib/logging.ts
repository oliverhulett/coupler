export class Logger {
    constructor(
        public verbose: boolean,
        public debug: boolean,
        public dump: boolean,
    ) {}

    function warn(args...) {
        console.log(args...);
    }

    function info(args...) {
        console.log(args...);
    }

    function verbose(args...) {
        if (this.verbose) {
            console.log(args...);
        }
    }

    function debug(args...) {
        if (this.debug) {
            console.log(args...);
        }
    }

    function dump(args...) {
        if (this.dump) {
            console.dir(args...);
        }
    }
}
