let argParser = require('minimist');
let dedent = require('dedent');
let fs = require('fs');
let path = require('path');

module.exports = {
    _printHelp: function(logger=console.log) {
        logger(dedent(`
            coupler <global options> obj[ect] <class or module or file name>
                Create (or update) a source object.  Coupler will discover fittings, ask questions, and update the filesystem under the CWD from the templates it finds and the answers you give.
                A source object is a collection of files named and pathed appropriately to represent a class or module or similar and its tests in the idiomatic style of the source tree in which
                the object lives.  As well as creating the files from appropriate templates, this command will hook them into the source tree's build tooling.
        `));
    },

    help: function(settings, argv) {
        this._printHelp();
        return 0;
    },

    main: function(settings, args) {
        settings.verbose(`Creating source object for project in ${settings.WORKING_DIR}/`);
        // parse args
        //none yet

        // find fittings

        // find existing project settings

        // Orchestrate grunt-init
        return 0;
    },
};
