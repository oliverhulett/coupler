/*
 * Template for a project from coupler.  Actually this should be delegating to the component templates under `root/`.
 * The fun happens when asking the questions.  After asking the general questions, about author's name and licenses, for example,
 * we then have to somehow discover the possible slots from `root/`, ask the user to determine which option they want for each slot,
 * and then delegate to the questions for those options.
 * Later, of course, we then have to delegate to those same options to set up the project.
 */

'use strict';

// Basic template description.
exports.description = 'Set up a project, later you need to also set up one or more source trees, and then any number of source objects.';

// Template-specific notes to be displayed before question prompts.
exports.notes = 'As well as the standard questions, you\'ll be asked to select the options for filling each slot and then delegated to all the questions to set-up each option.';

// Template-specific notes to be displayed after question prompts.
exports.after = 'Now that you\'ve set up a project, you can set up one or more source trees within it, and then any number of source objects within each source tree.';

// Any existing file or directory matching this wildcard will cause a warning.
exports.warnOn = '*';

// The actual init template.
exports.template = function(grunt, init, done) {
  init.process(/*defaults*/{}, /*prompts*/[
    // Prompt for these values.
    init.prompt('name'),
    init.prompt('description'),
    init.prompt('version'),
    init.prompt('repository'),
    init.prompt('homepage'),
    init.prompt('bugs'),
    init.prompt('licenses'),
    init.prompt('author_name'),
    init.prompt('author_email'),
    init.prompt('author_url'),
    // Delegate questions to the possible options for slots to make up a project.
  ], function(err, props) {
      console.log("err: " + err);
      console.log("props: " + props);

      // Delegate set-up to each of the chosen options, pass them your properties.

    // All done!
    done();
  });
};
