let argParser = require('../parse_args');
let dedent = require('dedent');
let fs = require('fs');
let path = require('path');

module.exports = {
    _printHelp: function(logger=console.log) {
        logger(dedent(`
            coupler <global options> project <local options> [+-]slot[:fitting]...
                Create (or update) a project.  Coupler will discover fittings, ask questions, and update the filesystem under the CWD from the templates it finds and the answers you give.
            Local Options:
                -o | --only  Create a project using only the requested slots, don't ask about fitting any other slots.
                -l | --list  List the defined slots and available fittings.
        `));
    },

    help: function(settings, argv) {
        this._printHelp();
        return 0;
    },

    main: function(settings, argv) {
        settings.verbose(`Creating project in ${settings.WORKING_DIR}`);
        // parse args
        // $ coupler project slot[:fitting]...
        // e.g. $ coupler project task-runner:invoke docker-distribution etc.
        args = argParser.parseArgs(
            argv,
            {
                only: {
                    boolean: true,
                    aliases: ['o'],
                },
                list: {
                    boolean: true,
                    aliases: ['l'],
                },
            },
        );

        // package run-time settings.  Command line arguments, user configuration/defaults, etc.
        requestedSlots = {};
        // TODO: read config files
        for (a in args['_']) {
            slot, fitting = a.split(':');
            if (!requestedSlots.hasOwnProperty(slot)) {
                requestedSlots[slot] = [];
            }
            requestedSlots[slot].push(fitting);
        }

        // find slots and fittings for projects.
        availableFittings = {};
        for (p in settings.PATH) {
            // TODO: walk p/project/... to build map of available slots to fittings
        }

        if (args.list) {
            this.listFittings(availableFittings, settings.info);
            return 0;
        }

        // Orchestrate grunt-init prompts
        projectFittings = this.orchestratePrompts(settings, availableFittings, requestedFittings, args.only);

        // Orchestrate grunt-init setup
        return this.orchestrateSetup(projectFittings);
    },

    listFittings: function(available, logger=console.log) {
        logger("Available Slots and Fittings:");
        for (s in available) {
            logger("  " + s);
            for (f in available[a]) {
                logger("    " + f);
                // TODO: Get description from template.
            }
            logger("");
        }
    },

    orchestratePrompts: function(settings, available, requested, onlyRequested=false) {
        projectSettings = {};
        for (slot in available) {
            fittings = available[slot];
            if (requested.hasOwnProperty(slot)) {
                if (requested[slot].length > 0) {
                    fittings = requested[slot];
                }
            } else {
                if (onlyRequested) {
                    settings.verbose(`Skipping slot due to --only flag: ${slot}`);
                    continue;
                }
            }
            // Ask how the user wants to fill this slot.  Select zero or more from available fittings.

            // For selected fittings, ask prompts for each fitting.
        }
        return projectSettings;
    },

    orchestrateSetup: function(fittings) {

    },
};
