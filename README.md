# Coupler

Coupler, a fitting used to join components together. (Ref: https://en.wikipedia.org/wiki/Scaffolding)

Coupler composes [grunt-init](https://gruntjs.com/project-scaffolding) templates.  Pick the ones you want, run them against a project directory.

## How It Works

First you want to create a project.  A project is made up of several slots (i.e. task-runner, docker distribution, formatter) that can be filled in various ways by "fittings".  Project creation will ask you questions about if or how you want to fill the project slots.
Within a project you can create any number of source trees.  A source tree is a for a particular language, it has a build tool, testing framework, etc.  Again the source tree creation will ask you questions about if or how you want to fill these slots.
Finally, you can create source objects within one of your source trees.  A source object is a generic term for a group of files that relate to the same project concept.  For example, a source file and a corresponding test file.

In many cases, you can re-run coupler on an existing entity and it will be updated with any new "best practices" that apply.  You should definitely commit the relavent files first though, so you can easily check the differences.

## Technicalities.

### Creating a Project

```
$ coupler project [options] slot[:fitting]...
```

There is a single coupler project template, running `coupler project` is more or less an alias `grunt-init templates/project`.  The options for filling project slots are stored under `templates/project/root`.  The magic is in the `templates/project/template.js`.  As well as doing all of the things that `coupler` does to compose project components, `templates/project/template.js` will delegate to the templates under `templates/project/root` chosen to fill the project slots.

### Adding a Source Tree

```
$ coupler tree [options] <name>
```

### Adding a Source Object

```
$ coupler obj[ect] [options] <name>
```

### Coupler PATH

The coupler search path is a list of directories in which coupler looks for slots and fittings.  Under each directory in the search path should be the following heirarchy.

```
{project,tree,object}/<slot>/<fitting>/<templates>
```

### Coupler Utils

The coupler utils package provides a way to store and retreive or infer answers so that prompts can be smarter and avoid repetition.  All templates should be setup relative to the top of the project.
