module.exports = {
    bflag(dash_arg, flag) {
        return (dash_arg ? ' ' + flag + ' ' : ' ')
    }
};
