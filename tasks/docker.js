let { run, options, help } = require('runjs');

let utils = require('./utils');


let REGISTRY_DEFAULT = '';
let IMAGE_DEFAULT = 'oliverhulett/coupler';
module.exports = [
    REGISTRY_DEFAULT,
    IMAGE_DEFAULT,
];

function build() {
    let name = REGISTRY_DEFAULT + IMAGE_DEFAULT;
    run('docker build --tag="' + name + ':dev" --tag="' + name + ':latest" .');
};
help(build, {
    description: 'Build the Coupler Docker image',
});
module.exports.build = build;

function push() {
    let myOpts = {
        tag: (options(this).tag && (typeof options(this).tag === "string")) ? options(this).tag : 'latest',
    };
    let name = REGISTRY_DEFAULT + IMAGE_DEFAULT;
    run('docker push "' + name + ':' + tag + '"');
};
help(push, {
    description: 'Push the Coupler Docker image',
    options: {
        tag: "Name of the tag to push",
    },
});
module.exports.push = push;

function tag() {
    let myOpts = {
        tag: (options(this).tag && (typeof options(this).tag === "string")) ? options(this).tag : VERSION,
    };
    let name = REGISTRY_DEFAULT + IMAGE_DEFAULT;
    run('docker tag "' + name + ':latest" "' + name + ':' + tag + '"');
};
help(tag, {
    description: 'Tag the latest Coupler Docker image',
    options: {
        tag: "Name of the tag to create",
    },
});
module.exports.tag = tag;

function runContainer() {
    let myOpts = {
        tag: (options(this).tag && (typeof options(this).tag === "string")) ? options(this).tag : 'dev',
    };
    run('TAG="' + myOpts.tag + '" bash ./run.sh');
};
help(runContainer, {
    description: 'Run the latest Coupler Docker image',
    options: {
        tag: "Name of the tag to run",
    },
});
module.exports.run = runContainer;

function clean() {
    let myOpts = {
        all: options(this).all || options(this).a,
        tag: (options(this).tag && (typeof options(this).tag === "string")) ? options(this).tag : false,
    };
    let name = REGISTRY_DEFAULT + IMAGE_DEFAULT;
    if (myOpts.tag) {
        run('docker rmi -f "' + name + ':' + tag + '"');
    } else {
        run('docker rmi -f "' + name + ':dev"');
        if (myOpts.all) {
            run('docker rmi -f "' + name + ':latest"');
        }
    }
};
help(clean, {
    description: 'Clean the Coupler Docker images',
    options: {
        a: "Clean development dependencies as well",
        all: "Clean development dependencies as well",
        tag: "Name of the tag to remove",
    },
});
module.exports.clean = clean;
