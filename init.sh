# shellcheck shell=bash
##
##	This file is intended to be sourced.  It will setup a development environment for this project.
##	This file is as idempotent as possible, so it should be able to be run multiple times without ill effect.  Subsequent runs should be very fast.
##
set -e

HERE="$(cd "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)"
#EXE="$(cd "$(dirname -- "$0")" && pwd -P)/$(basename -- "$0")"
#VENV_DIR="${HERE}/.venv"

GREEN="$(tput -T"${TERM:-dumb}" setaf 2 || true)"
WHITE="$(tput -T"${TERM:-dumb}" bold || true)$(tput -T"${TERM:-dumb}" setaf 7 || true)"
RESET="$(tput -T"${TERM:-dumb}" sgr0 || true)"
function _tell()
{
	echo "${WHITE} $* ${RESET}"
}
function _success()
{
	echo "${GREEN} $* ${RESET}"
}

_CHANGED="false"

if [ ! -d "${HERE}/node_modules" ] || [ ! -e "${HERE}/package-lock.json" ] || [ "${HERE}/package.json" -nt "${HERE}/package-lock.json" ]; then
	_tell "Installing nodejs development requirements: ${HERE}/package.json"
	_CHANGED="true"
	(cd "${HERE}" && npm install)
	if [ -e "${HERE}/package-lock.json" ]; then
		# Ensure package-lock.json is newer than package.json so that we don't run `npm install` unnecessarily.
		touch "${HERE}/package-lock.json"
	fi
fi

if [ "${_CHANGED}" == "true" ]; then
	# Don't advertise if we haven't changed anything, the dev env was already set up.
	_success "Coupler development environment set up"
fi
