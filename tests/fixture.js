let chai = require('chai');
chai.use(require('sinon-chai'));
require('mocha-sinon');

chai.config.includeStack = true; // turn on stack trace
chai.config.showDiff = true; // turn on reporter diff display
chai.config.truncateThreshold = 0; // disable truncating

let fs = require('fs');
let path = require('path');

module.exports = {
    lib: function(...args) {
        return path.join(path.dirname(fs.realpathSync(__filename)), '..', 'lib', ...args)
    },
}
