let chai = require('chai');
let sinon = require('sinon');

let fixture = require('../fixture');

describe("parse_args.js:", function() {
    before(function() {
        this.minimist = require('minimist');
        this.minimistSpy = sinon.spy(this.minimist);
        this.argParser = require(fixture.lib('./parse_args'));
    });

    describe("parseArgs():", function() {
        let result;
        let unknown = sinon.stub();
        describe("default options", function() {
            beforeEach(function () {
                result = this.argParser.parseArgs(
                    ['two', 'args'],
                );
            });
            it("should pass the default options", function() {
                chai.expect(result).to.deep.equal({'_': ['two', 'args']});
                console.log(this.minimistSpy.callCount);
                chai.expect(this.minimistSpy).to.be.calledWith(
                    ['two', 'args'],
                    {
                        stopEarly: false,
                        unknown: null,
                    },
                );
            });
        });
        describe("no expected args", function() {
            beforeEach(function () {
                result = this.argParser.parseArgs(
                    ['two', 'args'],
                );
            });
            it("should return the result object", function() {
                chai.expect(result).to.deep.equal({'_': ['two', 'args']});
            });
        });
        describe("expected arguments object", function() {
            let stubAction = sinon.stub();
            let stubMissing = sinon.stub();
            beforeEach(function() {
                result = this.argParser.parseArgs(
                    ['--flag', '-s', '--string=1', '--number', '2'],
                    {
                        flag: {
                            boolean: true,
                        },
                        'second-flag': {
                            boolean: true,
                            aliases: ['s'],
                        },
                        'missing-flag': {
                            boolean: true,
                        },
                        string: {
                            string: true,
                        },
                        number: {
                            action: (n) => { stubAction(n * 2) },
                        },
                        missing: {
                            onMissing: stubMissing,
                        },
                    },
                );
            });
            it("should return the result object", function() {
                chai.expect(result).to.deep.equal({
                    '_': [],
                    flag: true,
                    'second-flag': true,
                    s: true,
                    'missing-flag': false,
                    string: "1",
                    number: 2,
                });
            });
            it("should call the action for found arguments", function() {
                chai.expect(stubAction).to.be.calledWith(4);
            });
            it("should call the missing hook for missing arguments", function() {
                chai.expect(stubMissing).to.be.called;
            });
        });
    });
});
